#include<stdlib.h>
#include<time.h>
#include<unistd.h>
#include<stdio.h>

#include"car.h"

void car_init(car_t* car, int direction, bridge_t* bridge, int minspeed, int maxspeed){
	car -> bridge = bridge;
	car -> direction = direction > 0 ? 1 : -1;
	car -> speed = rand() % (minspeed - maxspeed + 1) + minspeed;
	car -> speed *= 10000;
	clock_gettime(CLOCK_REALTIME, &(car -> creation_time));
	sem_init(&(car -> can_move), 0, 0);
}

void* car_run(void* car_a){
	car_t* car = car_a;
	bridge_t* bridge = car -> bridge;
	int direction = car -> direction;
	unsigned int speed = car -> speed; 
	int* location = &(car -> location);
	int is_ambulance = car -> is_ambulance;

	sem_wait(&(car -> can_move)); /* wait for access to bridge */

	int first_tile = direction > 0 ? 0 : bridge -> size -1;
	sem_t* sem = direction > 0 ? &(bridge -> first) : &(bridge -> last);
	*location = first_tile;
	get_tile(bridge, *location, direction, is_ambulance);
	(*location) += direction;
	bridge -> direction = direction;

	usleep(speed);

	int is_first_cycle = 1;
	/* while car hasn't reached the last tile */
	while((*location < bridge -> size) && (*location >= 0)){
		get_tile(bridge, *location, direction, is_ambulance);
		release_tile(bridge, *location - direction);
		if(is_first_cycle){
			sem_post(sem);
			is_first_cycle = 0;
		}
		(*location) += direction;
		
		usleep(speed);
	}
	

	(*location) -= direction;
	release_tile(bridge, *location);
	sem_destroy(&(car-> can_move));
	pthread_mutex_lock(&(bridge -> counter_mutex));
	bridge->cars_inside--;
	pthread_mutex_unlock(&(bridge -> counter_mutex));
	free(car_a);
	
	return NULL;	
}
