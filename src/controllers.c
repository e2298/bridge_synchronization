#include"controllers.h"
#include"bridge.h"
#include"screen.h"
#include"car.h"
#include<math.h>
#include<stdlib.h>

void init(int bsize, int lambda_l, int lambda_r, int minspeed_l, int minspeed_r, int maxspeed_l, int maxspeed_r, int pambulance){
	srand(time(NULL));
	bridge_init(&bridge, bsize);
	screen_init(&bridge);

	queue_init(&queues[0]);
	pthread_mutex_init(&queues_mutex[0], NULL);
	queue_init(&queues[1]);
	pthread_mutex_init(&queues_mutex[1], NULL);
	maxspeeds[0] = maxspeed_l;
	maxspeeds[1] = maxspeed_r;
	minspeeds[0] = minspeed_l;
	minspeeds[1] = minspeed_r;
	lambdas[0] = lambda_l;
	lambdas[1] = lambda_r;
	p_ambulance = pambulance;
}

void start(int algo, int left, int right){
	int zero = 0, one = 1;
	pthread_t leftgen, rightgen;
	pthread_create(&leftgen, NULL, generator, &zero);
	pthread_create(&rightgen, NULL, generator, &one);
	
	/*FIFO*/
	if(algo == 1){
		while(1){
			int curr;

			while(!(queues -> size || (queues + 1) -> size));
			if(queues -> size) curr = 0;
			else curr = 1;
			if(queues[0].ambulances) curr = 0;
			if(queues[1].ambulances) curr = 1;

			while(bridge.cars_inside);
			while(((queues + curr) -> size || bridge.cars_inside) && (queues[curr].ambulances || !queues[curr ^ 1].ambulances)){
				car_t* next = peek(queues + curr);
				if(next){
					int fst = (next -> direction) > 0? 0 : bridge.size -1;
					sem_t* sem = (next -> direction) > 0? &(bridge.first) : &(bridge.last);
					sem_wait(sem);
					pthread_mutex_lock(queues_mutex + curr);
					dequeue(queues + curr);
					pthread_mutex_unlock(queues_mutex + curr);
					draw_queue(queues+curr, queues_mutex+curr, curr? -1 : 1);
					pthread_mutex_lock(&bridge.counter_mutex);
					bridge.cars_inside++;
					pthread_mutex_unlock(&bridge.counter_mutex);
					sem_post(&(next -> can_move));
				}
			}

		}
	}
	/*traffic lights*/
	else if(algo == 2){
		pthread_mutex_init(&lights_mutex, NULL);
		light_duration[0] = left;
		light_duration[1] = right;

		pthread_t sem_thread;
		pthread_create(&sem_thread, NULL, run_lights, NULL);
		pthread_t ambulance_check;
		pthread_create(&ambulance_check, NULL, lights_check_ambulance, NULL);
		while(1){
			int curr = right_of_way;
			int should_cont = 0;
			if(bridge.direction == (curr? 1 : -1))while(bridge.cars_inside)if(right_of_way != curr){should_cont = 1; break;} ;
			if(should_cont) continue;
			while(right_of_way == curr){
				car_t* next = peek(queues + curr);
				if(next){
					int fst = (next -> direction) > 0? 0 : bridge.size -1;
					sem_t* sem = (next -> direction) > 0? &(bridge.first) : &(bridge.last);
					sem_wait(sem);
					pthread_mutex_lock(queues_mutex + curr);
					dequeue(queues + curr);
					pthread_mutex_unlock(queues_mutex + curr);
					draw_queue(queues+curr, queues_mutex+curr, curr? -1 : 1);
					pthread_mutex_lock(&bridge.counter_mutex);
					bridge.cars_inside++;
					pthread_mutex_unlock(&bridge.counter_mutex);
					sem_post(&(next -> can_move));
				}
			}
		}
	}
	/*traffic police*/
	else{
		cars_passed[0] = left;
		cars_passed[1] = right;
		int curr = 1;

		void handle_ambulance(){
			int curro = curr;
			if(queues[curr ^ 1].ambulances && !queues[curr].ambulances){
				curr = curr ^ 1;
				while(bridge.cars_inside);	
			}
			while(queues[curr].ambulances){
				car_t* next = peek(queues + curr);
				if(next){
					int fst = (next -> direction) > 0? 0 : bridge.size -1;
					sem_t* sem = (next -> direction) > 0? &(bridge.first) : &(bridge.last);
					sem_wait(sem);
					pthread_mutex_lock(queues_mutex + curr);
					dequeue(queues + curr);
					pthread_mutex_unlock(queues_mutex + curr);
					draw_queue(queues+curr, queues_mutex+curr, curr? -1 : 1);
					pthread_mutex_lock(&bridge.counter_mutex);
					bridge.cars_inside++;
					pthread_mutex_unlock(&bridge.counter_mutex);
					sem_post(&(next -> can_move));
				}
			}	
			if(curro != curr){
				while(bridge.cars_inside);	
				curr = curro;
			}
			return;
		}

		while(1){
			while(bridge.cars_inside) if(queues[0].ambulances || queues[1].ambulances) handle_ambulance();
			curr ^= 1;
			int cars_left = cars_passed[curr];
			while(cars_left){
				if(queues[0].ambulances || queues[1].ambulances) handle_ambulance();
				car_t* next = peek(queues + curr);
				if(next){
					cars_left--;
					int fst = (next -> direction) > 0? 0 : bridge.size -1;
					sem_t* sem = (next -> direction) > 0? &(bridge.first) : &(bridge.last);
					sem_wait(sem);
					pthread_mutex_lock(queues_mutex + curr);
					dequeue(queues + curr);
					pthread_mutex_unlock(queues_mutex + curr);
					draw_queue(queues+curr, queues_mutex+curr, curr? -1 : 1);
					pthread_mutex_lock(&bridge.counter_mutex);
					bridge.cars_inside++;
					pthread_mutex_unlock(&bridge.counter_mutex);
					sem_post(&(next -> can_move));
				}
			}
		}
	}

	while(1);
}

void set_lights(){
	pthread_mutex_lock(&screen_lock);
	attron(COLOR_PAIR(4 - right_of_way));
	move(bridge.screen_y-1, bridge.screen_x);
	addch('o');
	attron(COLOR_PAIR(3 + right_of_way));
	move(bridge.screen_y-1, bridge.screen_x + bridge.size - 1);
	addch('o');
	attron(COLOR_PAIR(2));
	pthread_mutex_unlock(&screen_lock);
}

/*run_as_thread*/
void* run_lights(void* trash){
	right_of_way = 0;
	set_lights();

	light_duration[0] *= 10000;
	light_duration[1] *= 10000;

	while(1){
		usleep(light_duration[right_of_way]);
		pthread_mutex_lock(&lights_mutex);
		right_of_way ^= 1;
		set_lights();
		pthread_mutex_unlock(&lights_mutex);
	}
}

/*run as thread*/
void* lights_check_ambulance(void* trash){
	while(1){
		while(!(queues[0].ambulances || queues[1].ambulances));
		pthread_mutex_lock(&lights_mutex);
		if(queues[right_of_way].ambulances);
		else right_of_way ^= 1;
		set_lights();
		while(queues[right_of_way].ambulances);

		pthread_mutex_unlock(&lights_mutex);
	}
}


void* generator(void* side_p){
	int side = *(int*)side_p;
	int direction = side? -1 : 1;
	int minspeed = minspeeds[side];
	int maxspeed = maxspeeds[side];
	queue_t* queue = &queues[side];
	int lambda = lambdas[side];
	pthread_mutex_t* mutex = &queues_mutex[side];

	while(1){
		/* generate random wait time and wait it */
		int sleept = rand();
		if(!sleept) sleept = 1;
		sleept = log((double)sleept/(double)RAND_MAX) * -(double)lambda;
		//printf("%d\n", sleept);
		sleept*= 10000;
		usleep(sleept);

		/* create car */
		car_t* car = malloc(sizeof(car_t));
		car_init(car, direction, &bridge, minspeed, maxspeed);
		int is_ambulance = (rand() % 100) <= p_ambulance;
		car -> is_ambulance = is_ambulance;
		pthread_t trash;

		pthread_create(&trash, NULL, car_run, car);
		
		pthread_mutex_lock(mutex);
		enqueue(queue, car);
		pthread_mutex_unlock(mutex);
		draw_queue(queue, mutex, direction);
		//sem_post(&(car -> can_move));
	}
}
