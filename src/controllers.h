#ifndef CONTROLLERS_H
#define CONTROLLERS_H
#include<pthread.h>
#include"car_queue.h"

pthread_mutex_t queues_mutex[2];
queue_t queues[2];
int lambdas[2];
int minspeeds[2];
int maxspeeds[2];
bridge_t bridge;
int p_ambulance;
int right_of_way; /*0 means left can go, 1 right (for use with traffic lights)*/
int light_duration[2];
pthread_mutex_t lights_mutex;
int cars_passed[2];

int gen_lambda;


/*inits everything but the simulation*/
void init(int bsize, int lambda_l, int lambda_r, int minspeed_l, int minspeed_r, int maxspeed_l, int maxspeed_r, int pambulance);

void start(int algo, int left, int right);

void* run_lights(void*);

void* lights_check_ambulance(void*);

void* generator(void* side_p);

#endif
