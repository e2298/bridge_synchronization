#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<pthread.h>
#include"controllers.h"
#include"car.h"

int main(void){
	int bsize, lambda_l, lambda_r, minspeed_l, maxspeed_l, minspeed_r, maxspeed_r, pambulance;
	FILE* inp = fopen("input", "r");
	if(!inp){
		printf("El archivo de entrada no existe\n");
		exit(1);
	}
	fscanf(inp,"%d %d %d %d %d %d %d %d", &bsize, &lambda_l, &lambda_r, &minspeed_l, &maxspeed_l , &minspeed_r, &maxspeed_r, &pambulance);
	init(bsize, lambda_l, lambda_r, minspeed_l, minspeed_r, maxspeed_l, maxspeed_r, pambulance);

	int algo;
	fscanf(inp,"%d", &algo);
	if(algo == 1)
		start(1, 0, 0);
	else{
		int a,b;
		fscanf(inp, "%d %d", &a, &b);
		start(algo, a, b);
	}

	return 0;
}
