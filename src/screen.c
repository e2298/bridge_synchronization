#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include"screen.h"
#include"controllers.h"

void screen_init(bridge_t* bridge){
	pthread_mutex_init(&screen_lock, NULL);

	screen = initscr();
	int size = bridge -> size;
	int maxx, maxy;
	int bridge_x, bridge_y;
	getmaxyx(stdscr, maxy, maxx);
	bridge_y = maxy/2;
	bridge_x = maxx/2;
	bridge_x -= size/2;	

	if(bridge_x < 0){
		fprintf(stderr,"Error: la pantalla no es lo suficientemente grande");
		exit(1);
	}

	bridge -> screen_x = bridge_x;
	bridge -> screen_y = bridge_y;

	move(bridge_y, bridge_x);
	while(size--){
		addch('=');
	}
	curs_set(0);
	noecho();
	nodelay(stdscr,1);
	start_color();
	init_pair(1, COLOR_RED, COLOR_WHITE); /*ambulance*/
	init_pair(2, COLOR_WHITE, COLOR_BLACK); /*normal */
	init_pair(3, COLOR_RED, COLOR_BLACK); /*red light*/
	init_pair(4, COLOR_GREEN, COLOR_BLACK); /*green light*/

	pthread_t scr_thread;
	pthread_create(&scr_thread, NULL, screen_handler, NULL);
}

void* screen_handler(void* throwaway){
	int inp;
	while(1){
		pthread_mutex_lock(&screen_lock);
		refresh();
		pthread_mutex_unlock(&screen_lock);

		usleep(10000);
		
		inp = getch();
		if(inp != -1)
			exit(0);
	}
}

void draw_queue(queue_t* q, pthread_mutex_t* mutex, int side){
	int maxx, maxy;
	getmaxyx(stdscr, maxy, maxx);
	int x, y;

	pthread_mutex_lock(&screen_lock);
	pthread_mutex_lock(mutex);
	if(side > 0){
		for(y = bridge.screen_y -2; y >= 0; y-= 2){
			move(y,0);
			for(x = 0; x < maxx; x++){
				addch(' ');
			}
		} 
		move(bridge.screen_y,0);
		for(x = 0; x < bridge.screen_x; x++){
			addch(' ');
		}

		x = bridge.screen_x -2;
		y = bridge.screen_y;
		queue_node* curr = q -> head;
		while(curr != NULL){
			while(x > -1 && curr != NULL){
				move(y,x);
				if(curr -> car -> is_ambulance) attron(COLOR_PAIR(1));
				addch('>');
				if(curr -> car -> is_ambulance) attron(COLOR_PAIR(2));
				curr = curr -> next;
				x--;
			}
			x = 0;
			y-= 2;
			while(x < maxx && curr != NULL){
				move(y,x);
				if(curr -> car -> is_ambulance) attron(COLOR_PAIR(1));
				addch('<');
				if(curr -> car -> is_ambulance) attron(COLOR_PAIR(2));
				curr = curr -> next;
				x++;
			}
			x = maxx-1;
			y-= 2;
		}
		
	}
	else{ 
		move(bridge.screen_y, bridge.screen_x + bridge.size + 1);
		for(x = bridge.screen_x + bridge.size + 1; x < maxx; x++){
			addch(' ');
		}
		for(y = bridge.screen_y + 2; y < maxy; y += 2){
			move(y,0);
			for(x = 0; x < maxx; x++){
				addch(' ');
			}
		} 
	
		x = bridge.screen_x + bridge.size + 1;;
		y = bridge.screen_y;
		queue_node* curr = q -> head;
		while(curr != NULL){
			while(x < maxx && curr != NULL){
				move(y,x);
				if(curr -> car -> is_ambulance) attron(COLOR_PAIR(1));
				addch('<');
				if(curr -> car -> is_ambulance) attron(COLOR_PAIR(2));
				curr = curr -> next;
				x++;
			}
			x = maxx-1;
			y+= 2;
			while(x > -1 && curr != NULL){
				move(y,x);
				if(curr -> car -> is_ambulance) attron(COLOR_PAIR(1));
				addch('>');
				if(curr -> car -> is_ambulance) attron(COLOR_PAIR(2));
				curr = curr -> next;
				x--;
			}
			x = 0;
			y+= 2;
		}
	}
	pthread_mutex_unlock(&screen_lock);
	pthread_mutex_unlock(mutex);
}

