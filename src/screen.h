#ifndef SCREEN_H
#define SCREEN_H
#include<curses.h>
#include<pthread.h>
#include"car_queue.h"
#include"bridge.h"

WINDOW* screen;
pthread_mutex_t screen_lock;

void screen_init(bridge_t* bridge);

/* periodically redraws the screen, and checks for quit command */
void* screen_handler(void*);

/* 1 is left, -1 right */
void draw_queue(queue_t* q, pthread_mutex_t* mutex, int side);

#endif
