#include<stdlib.h>
#include"bridge.h"
#include"screen.h"

void bridge_init(bridge_t* bridge, int size){
	int i;
	bridge -> size = size;
	bridge -> tiles = malloc(size * sizeof(pthread_mutex_t));
	for(i = 0; i < size; i++){
		pthread_mutex_init(&(bridge -> tiles[i]), NULL);
	}
	pthread_mutex_init(&(bridge -> counter_mutex), NULL);
	bridge -> cars_inside = 0;
	bridge -> direction = 0;
	sem_init(&(bridge -> first), 0, 1);
	sem_init(&(bridge -> last), 0, 1);
}

void get_tile(bridge_t* bridge, int tile_no, int direction, int is_ambulance){
	pthread_mutex_lock(bridge -> tiles + tile_no);
	
	/*draw stuff*/
	pthread_mutex_lock(&screen_lock);
	move(bridge -> screen_y, bridge -> screen_x+tile_no);
				if(is_ambulance) attron(COLOR_PAIR(1));
	addch(direction > 0? '>' : '<');	
				if(is_ambulance) attron(COLOR_PAIR(2));
	pthread_mutex_unlock(&screen_lock);
}

void release_tile(bridge_t* bridge, int tile_no){
	/*draw stuff*/
	pthread_mutex_lock(&screen_lock);
	move(bridge -> screen_y, bridge -> screen_x + tile_no);
	addch('=');
	pthread_mutex_unlock(&screen_lock);

	pthread_mutex_unlock(bridge -> tiles + tile_no);
}
