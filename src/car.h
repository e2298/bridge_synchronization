#ifndef CAR_H
#define CAR_H
#include"bridge.h"
#include<semaphore.h>

typedef struct{
	bridge_t* bridge;
	int direction;/* positive is left to right, negative right to left */
	unsigned int speed; /* nanoseconds between movements */
	int location;/* location on bridge */
	struct timespec creation_time;
	sem_t can_move;
	int is_ambulance;
} car_t;

/* positive direction means left to right 
 * speeds are in tiles/hundreths of a second
 * */
void car_init(car_t* car, int direction, bridge_t* bridge, int minspeed, int maxspeed); 

/* car's life cycle
 * controlling thread must lock can_move before starting car,
 * as releasing it grants access to the bridge */
void* car_run(void* car);

#endif

