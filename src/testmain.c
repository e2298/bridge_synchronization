#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<pthread.h>
#include"bridge.h"
#include"screen.h"
#include"car.h"

int main(void){
	srand(time(NULL));

	bridge_t bridge;	
	bridge_init(&bridge, 10);
	screen_init(&bridge);
	car_t car;
	car_init(&car, 1, &bridge);
	pthread_t th;
	pthread_create(&th, NULL, car_run, &car);
	pthread_join(th, NULL);
	while(1);

	return 0;
}
