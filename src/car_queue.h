#ifndef CAR_Q_H
#define CAR_Q_H
#include"car.h"

typedef struct _queue_node queue_node;

struct _queue_node{
	car_t* car;
	queue_node* next;
};

typedef struct{
	queue_node* head;
	queue_node* back;
	unsigned int ambulances;
	unsigned int size;
} queue_t;

void queue_init(queue_t* q);

void enqueue(queue_t* q, car_t* car);

car_t* dequeue(queue_t* q);

car_t* peek(queue_t* q);

#endif
