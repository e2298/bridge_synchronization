#ifndef BRIDGE_H
#define BRIDGE_H
#include<pthread.h>
#include<semaphore.h>

/* a set of tiles, each car must get a lock on the tile in front, move, and release the lock on the tile behind */
typedef struct{
	unsigned int size;
	pthread_mutex_t* tiles;
	volatile unsigned int cars_inside; /* traffic controller should increment this before unlocking a car, and each car should decrement it */
	pthread_mutex_t counter_mutex;
	int direction;
	int screen_x, screen_y;
	sem_t first;
	sem_t last;
} bridge_t;

void bridge_init(bridge_t* bridge, int size);

/* gets lock and draws car
 * ambulance is 'bool'
 * positive direction means left to right, negative otherwise
 */
void get_tile(bridge_t* bridge, int tile_no, int direction, int is_ambulance);

/* releases lock and draws over car
 */
void release_tile(bridge_t* bridge, int tile_no);

#endif
