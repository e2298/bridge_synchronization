#include"car_queue.h"
#include<stdlib.h>

void queue_init(queue_t* q){
	q -> head = NULL;
	q -> back = NULL;
	q -> ambulances = 0;
	q -> size = 0;
}

void enqueue(queue_t* q, car_t* car){
	queue_node* new = malloc(sizeof(queue_node));		
	new -> car = car;
	new -> next = NULL;

	if(q -> back)
		q -> back -> next = new;
	else
		q -> head = new;

	q -> back = new;

	if(car -> is_ambulance) q -> ambulances++;
	(q -> size)++;
}

car_t* dequeue(queue_t* q){
	if(!q -> head)
		return NULL;

	queue_node* curr = q -> head;

	q -> head = curr -> next;
	if(!(curr -> next))
		q -> back = NULL;

	if(curr -> car -> is_ambulance) q -> ambulances--;
	(q -> size)--;

	return curr -> car;
}

car_t* peek(queue_t* q){
	if(q -> head)
		return q -> head -> car;
	else 
		return NULL;
}
