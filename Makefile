EXEC = bridge
CFILES = main.c car.c bridge.c car_queue.c screen.c controllers.c
CFILES := $(addprefix src/,$(CFILES))
HEADERS = car.h bridge.h car_queue.h screen.h controllers.h
HEADERS := $(addprefix src/,$(HEADERS))
CC = gcc -std=gnu90
COPTIONS = -o $(EXEC) -lncurses -lpthread -lm

all: run clean

$(EXEC): $(CFILES) $(HEADERS)
	$(CC) $(CFILES) $(COPTIONS)

.PHONY: clean run

run: $(EXEC)
	./$(EXEC) 

clean: 
	rm $(EXEC)

